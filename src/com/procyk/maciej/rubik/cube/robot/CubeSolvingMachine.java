package com.procyk.maciej.rubik.cube.robot;

import com.procyk.maciej.rubik.cube.model.CubeMove;

public class CubeSolvingMachine {
	
	private CubeRotator cubeRotator;
	private CubeFlipper cubeFlipper;
	
	private Face currentFaceUp = Face.UP;
	private int currentRotation = 0;
	
	private static final int DEG_0 = 0;
	private static final int DEG_90 = 1;
	private static final int DEG_180 = 2;
	private static final int DEG_270 = 3;

	private static final boolean DEBUG = false;
	
	public CubeSolvingMachine(CubeRotator cubeRotator, CubeFlipper cubeFlipper) {
		this.cubeRotator = cubeRotator;
		this.cubeFlipper = cubeFlipper;
	}

	public void solveWithMoves(CubeMove[] moves) {
		this.cubeFlipper.positionCalibration();
		for (CubeMove move : moves)
			moveFor(move);
	}

	protected void moveFor(CubeMove move) {
		switch (move) {
			case F: F();  break;
			case G: FP(); break;
			case H: F2(); break;
			case R: R();  break;
			case T: RP(); break;
			case Y: R2(); break;
			case U: U();  break;
			case I: UP(); break;
			case O: U2(); break;
		}
	}

	private void U() {
		U_help(1);
	}

	private void UP() {
		U_help(-1);
	}

	private void U2() {
		U_help(2);
	}

	private void F() {
		F_help(1);
	}

	private void FP() {
		F_help(-1);
	}

	private void F2() {
		F_help(2);
	}

	private void R() {
		R_help(1);
	}

	private void RP() {
		R_help(-1);
	}

	private void R2() {
		R_help(2);
	}
	
	private void U_help(int rotate) {
		debug();
		switch (currentFaceUp) {
			case UP: {
				rotateOnProperFaceUp(rotate);
				return;
			}
			case DOWN: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_0);
				cubeFlipper.flipCubeAndCover();
				cubeFlipper.flipCubeAndCover();
			} break;
			case FRONT: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_270);
				cubeFlipper.flipCubeAndCover();
			} break;
			case LEFT: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_180);
				cubeFlipper.flipCubeAndCover();
			} break;
			case BACK: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_90);
				cubeFlipper.flipCubeAndCover();
			} break;
			case RIGHT: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_0);
				cubeFlipper.flipCubeAndCover();

			} break;
		}
		currentFaceUp = Face.UP;
		U_help(rotate);
	}

	private void R_help(int rotate) {
		debug();
		switch (currentFaceUp) {
			case RIGHT: {
				rotateOnProperFaceUp(rotate);
				return;
			}
			case DOWN: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_0);
				cubeFlipper.flipCubeAndCover();
			} break;
			case FRONT: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_180);
				cubeFlipper.flipCubeAndCover();
				currentRotation = DEG_270;
			} break;
			case LEFT: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_0);
				cubeFlipper.flipCubeAndCover();
				cubeFlipper.flipCubeAndCover();
			} break;
			case BACK: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_180);
				cubeFlipper.flipCubeAndCover();
				currentRotation = DEG_90;
			} break;
			case UP: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_180);
				cubeFlipper.flipCubeAndCover();
			} break;
		}
		currentFaceUp = Face.RIGHT;
		R_help(rotate);
	}

	private void F_help(int rotate) {
		debug();
		switch (currentFaceUp) {
			case FRONT: {
				rotateOnProperFaceUp(rotate);
				return;
			}
			case DOWN: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_90);
				cubeFlipper.flipCubeAndCover();
				currentRotation = DEG_270;
			} break;
			case RIGHT: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_90);
				cubeFlipper.flipCubeAndCover();
				currentRotation = DEG_0;
			} break;
			case LEFT: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_90);
				cubeFlipper.flipCubeAndCover();
				currentRotation = DEG_180;
			} break;
			case BACK: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_90);
				cubeFlipper.flipCubeAndCover();
				cubeFlipper.flipCubeAndCover();
			} break;
			case UP: {
				cubeFlipper.releaseCube();
				this.rotateUntilRotation(DEG_90);
				cubeFlipper.flipCubeAndCover();
			} break;
		}
		currentFaceUp = Face.FRONT;
		F_help(rotate);
	}

	private void rotateOnProperFaceUp(int rotate) {
		cubeFlipper.coverCube();
		switch (rotate) {
			case 1: {
				cubeRotator.leftRotate(true);
				currentRotation = (currentRotation + 3) % 4;
			} break;
			case -1: {
				cubeRotator.rightRotate(true);
				currentRotation = (currentRotation + 1) % 4;
			} break;
			case 2: {
				cubeRotator.fullRotate(true);
				currentRotation = (currentRotation + 2) % 4;
			} break;
			default:
				throw new IllegalArgumentException("bad rotate parameter to U_help function");
		}
	}

	private void rotateUntilRotation(int rotation) {
		int difference = (currentRotation - rotation + 4) % 4;
		cubeFlipper.releaseCube();
		switch (difference) {
			case 1:
				cubeRotator.leftRotate(false);
				break;
			case 2:
				cubeRotator.fullRotate(false);
				break;
			case 3:
				cubeRotator.rightRotate(false);
				break;
		}
		currentRotation = rotation;
	}

	private void debug() {
		if (DEBUG) {
			System.out.println(currentFaceUp);
			System.out.println(currentRotation*90);
		}
	}
	
	private enum Face { UP, DOWN, RIGHT, LEFT, FRONT, BACK }
}
