package com.procyk.maciej.rubik.cube.robot;
import lejos.nxt.NXTRegulatedMotor;

public abstract class CubePositioner {
	
	protected final NXTRegulatedMotor moveMotor;
	protected final boolean changeDirection;
	
	public CubePositioner(NXTRegulatedMotor motor, boolean changeDirection) {
		this.moveMotor = motor;
		this.changeDirection = changeDirection;
	}
	
	public abstract void positionCalibration();
	
	public void stop() {
		this.moveMotor.stop();
	}
	
	public void flt() {
		this.moveMotor.flt();
	}
	
	protected int decideDirection(int angle) {
		return !this.changeDirection ? angle : -angle;
	}
}
