package com.procyk.maciej.rubik.cube.robot;

public interface IForCubeChecker {
	boolean isCubePresent();
}
