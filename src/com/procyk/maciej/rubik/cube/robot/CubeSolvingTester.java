package com.procyk.maciej.rubik.cube.robot;

import com.procyk.maciej.rubik.cube.model.CubeMove;

import java.util.Random;

public class CubeSolvingTester extends CubeSolvingMachine {

    private static final Random GENERATOR = new Random();

    public CubeSolvingTester(CubeRotator cubeRotator, CubeFlipper cubeFlipper) {
        super(cubeRotator, cubeFlipper);
    }

    public void testWithRandomMoves() {
        while (true) {
            this.moveFor(CubeMove.values()[GENERATOR.nextInt(CubeMove.values().length)]);
        }
    }
}
