package com.procyk.maciej.rubik.cube.robot;
import lejos.nxt.NXTRegulatedMotor;

public class CubeRotator extends CubePositioner {

	private static final int ROTATION_SPEED = 160;
	private static final int ORIENTATION_CHANGED = 60;
	private static final int DEG_90 = 90;
	private static final int DEG_180 = 180;
	private static final int DEG_CALIBRE_MOVE = 13;
	private static final int CALIBRATION_SPEED = 150;

	public  CubeRotator(boolean changeRotation, NXTRegulatedMotor motor) {
		super(motor, changeRotation);
	}
	
	public CubeRotator(NXTRegulatedMotor motor) {
		this(false, motor);
	}
	
	public void changeOrientation() {
		helpRotate(ORIENTATION_CHANGED, 0);
	}
	
	public void backOrientation() {
		helpRotate(-ORIENTATION_CHANGED, 0);
	}
	
	public void leftRotate(boolean calibre) {
		helpRotate(DEG_90, calibre ? DEG_CALIBRE_MOVE : 0);
	}
	
	public void rightRotate(boolean calibre) {
		helpRotate(-DEG_90, calibre ? -DEG_CALIBRE_MOVE : 0);
	}
	
	public void fullRotate(boolean calibre) {
		helpRotate(DEG_180, calibre ? DEG_CALIBRE_MOVE : 0);
	}
	
	private void helpRotate(int angle, int calibreAngle) {
		this.moveMotor.setSpeed(ROTATION_SPEED);
		this.moveMotor.rotate(decideDirection(angle + calibreAngle));
		if (calibreAngle != 0) {
			this.moveMotor.setSpeed(CALIBRATION_SPEED);
			this.moveMotor.rotate(decideDirection(-(2 * calibreAngle)));
			this.moveMotor.rotate(decideDirection(2 * calibreAngle));
			this.moveMotor.rotate(decideDirection(-calibreAngle));
//			this.moveMotor.rotate(decideDirection(-(calibreAngle)));
		}
		this.moveMotor.stop();
	}
	
	@Override
	public void positionCalibration() { }
}
