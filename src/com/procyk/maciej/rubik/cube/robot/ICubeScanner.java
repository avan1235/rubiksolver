package com.procyk.maciej.rubik.cube.robot;

import com.procyk.maciej.rubik.cube.model.Cube;

public interface ICubeScanner extends ICubePositioner {
	void scan(Cube cube);
}
