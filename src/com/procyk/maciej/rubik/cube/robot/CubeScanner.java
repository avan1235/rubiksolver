package com.procyk.maciej.rubik.cube.robot;

import com.procyk.maciej.rubik.cube.model.Cube;
import com.procyk.maciej.rubik.cube.model.CubeColor;
import com.procyk.maciej.rubik.cube.model.CubeColor.NotDetectedException;

import lejos.nxt.ColorSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.ColorSensor.Color;
import lejos.nxt.NXTRegulatedMotor;
import lejos.util.Delay;

public class CubeScanner extends CubePositioner implements ICubeScanner {

	private ColorSensor colorSensor;
	private CubeRotator cubeRotator;
	private CubeFlipper cubeFlipper;

	private static final int SENSOR_MOTOR_ANGLE = -220;
	private static final int SENSOR_MOTOR_SPEED = 400;
	private static final int CALIBRATION_TIME_MOVE = 1000;
	private static final int CALIBRATION_SPEED = 200;
	private static final int SENSOR_MOTOR_CALIBRATION_SPEED = 250;
	
	public CubeScanner(SensorPort colorSensorPort, CubeRotator cubeRotator, CubeFlipper cubeFlipper, NXTRegulatedMotor sensorMotor) {
		this(colorSensorPort, cubeRotator, cubeFlipper, sensorMotor, false);
	}
	
	public CubeScanner(SensorPort colorSensorPort, CubeRotator cubeRotator, CubeFlipper cubeFlipper, NXTRegulatedMotor sensorMotor, boolean changeDirection) {
		super(sensorMotor, changeDirection);
		this.cubeFlipper = cubeFlipper;
		this.cubeRotator = cubeRotator;
		this.colorSensor = new ColorSensor(colorSensorPort);
	}
	
	public void scan(Cube cube) {
		cubeFlipper.releaseCube();
		this.positionCalibration();
		colorSensor.setFloodlight(Color.WHITE);
		
		singleFaceMeasure(cube, 8, 0);
		cubeFlipper.flipCubeAndRelease();
		
		singleFaceMeasure(cube, 20, 2);
		cubeFlipper.flipCubeAndRelease();
		
		singleFaceMeasure(cube, 12, 2);
		cubeFlipper.flipCubeAndRelease();
		
		singleFaceMeasure(cube, 4, 1);
		cubeFlipper.flipCubeAndRelease();
		cubeRotator.leftRotate(false);
		cubeFlipper.flipCubeAndRelease();
		
		singleFaceMeasure(cube, 16, 1);
		cubeFlipper.flipCubeAndCover();
		cubeFlipper.flipCubeAndRelease();
		
		singleFaceMeasure(cube, 0, 1);
		cubeFlipper.flipCubeAndRelease();
		cubeRotator.rightRotate(false);
		
		colorSensor.setFloodlight(false);
	}
	
	private void singleFaceMeasure(Cube cube, int start, int offset) {
		beforeMeasure();
		for (int i = 0; i < 4; i++) 
			pickColorFor(cube, start + ((i + offset) % 4));
		afterMeasure();
	}
	
	private void pickColorFor(Cube cube, int position) {
		CubeColor cubeColor = null;
		Color color = null;
		while (cubeColor == null) {
			try {
				color = colorSensor.getRawColor();
				cubeColor = CubeColor.detect(color);
			} catch (NotDetectedException exception) { 
				System.out.println("Color not detected");
				System.out.println("R:" + color.getRed() + "G:" + color.getGreen() + "B:" + color.getBlue());
			}
		}
		cube.setColor(position, cubeColor);
		cubeRotator.leftRotate(false);
	}
	
	private void beforeMeasure() {
		moveMotor.setSpeed(SENSOR_MOTOR_SPEED);
		moveMotor.rotate(decideDirection(SENSOR_MOTOR_ANGLE));
		moveMotor.stop();
		cubeRotator.changeOrientation();
	}
	
	private void afterMeasure() {
		cubeRotator.backOrientation();
		moveMotor.setSpeed(SENSOR_MOTOR_SPEED);
		moveMotor.rotate(decideDirection(-SENSOR_MOTOR_ANGLE));
		moveMotor.stop();
	}

	@Override
	public void positionCalibration() {
		this.moveMotor.setSpeed(CALIBRATION_SPEED);
		if (!this.changeDirection) this.moveMotor.forward(); else this.moveMotor.backward();
		Delay.msDelay(CALIBRATION_TIME_MOVE);
		this.moveMotor.stop();
		this.moveMotor.flt();
		this.moveMotor.stop();
	}
}
