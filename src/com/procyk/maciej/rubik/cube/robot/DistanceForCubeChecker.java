package com.procyk.maciej.rubik.cube.robot;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;

public class DistanceForCubeChecker implements IForCubeChecker {

	private final UltrasonicSensor sensor;
	private final int minDistanceCm;
	
	public DistanceForCubeChecker(SensorPort port, int minDistanceCm) {
		this.sensor = new UltrasonicSensor(port);
		this.minDistanceCm = minDistanceCm;
		System.out.println("Wait for cube...");
	}
	
	@Override
	public boolean isCubePresent() {
		sensor.continuous();
		boolean result = false;
		if (sensor.getDistance() < this.minDistanceCm) {
			result = true;
			System.out.println("Cube present");
			Sound.beep();
		}
		sensor.off();
		return result;
	}

}
