package com.procyk.maciej.rubik.cube.robot;
import lejos.nxt.NXTRegulatedMotor;
import lejos.util.Delay;

public class CubeFlipper extends CubePositioner {
	
	private enum CurrentFlipState {
		RELEASED,
		ON_CUBE;
	}
	
	private static CurrentFlipState currentFlipState = CurrentFlipState.ON_CUBE;
	
	private static final int FLIP_SPEED = 130;
	private static final int RELEASE_SPEED = 170;
	private static final int CALIBRATION_SPEED = 70;
	
	private static final int FULL_FLIP_ANGLE = 150;
	private static final int COVER_ANGLE_TO_RELEASE = 60;
	private static final int CALIBRE_ANGLE = 15;

	private static final long CALIBRATION_TIME_MOVE = 2500;

	public CubeFlipper(NXTRegulatedMotor moveMotor) {
		this(false, moveMotor);
	}
	
	public CubeFlipper(boolean changeDirection, NXTRegulatedMotor moveMotor) {
		super(moveMotor, changeDirection);
	}
	
	public void flipCubeAndRelease() {
		switch (currentFlipState) {
			case RELEASED: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(FULL_FLIP_ANGLE));
				this.moveMotor.setSpeed(RELEASE_SPEED);
				this.moveMotor.rotate(decideDirection(-FULL_FLIP_ANGLE));
				this.moveMotor.stop();
			} break;
			case ON_CUBE: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(FULL_FLIP_ANGLE - COVER_ANGLE_TO_RELEASE - CALIBRE_ANGLE));
				this.moveMotor.setSpeed(RELEASE_SPEED);
				this.moveMotor.rotate(decideDirection(-FULL_FLIP_ANGLE));
				this.moveMotor.stop();
			} break;
		}
		currentFlipState = CurrentFlipState.RELEASED;
	}
	
	public void flipCubeAndCover() {
		switch (currentFlipState) {
			case RELEASED: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(FULL_FLIP_ANGLE));
				this.moveMotor.rotate(decideDirection(-(FULL_FLIP_ANGLE - COVER_ANGLE_TO_RELEASE)));
				this.moveMotor.rotate(decideDirection(CALIBRE_ANGLE));
				this.moveMotor.stop();
			} break;
			case ON_CUBE: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(FULL_FLIP_ANGLE - COVER_ANGLE_TO_RELEASE - CALIBRE_ANGLE));
				this.moveMotor.rotate(decideDirection(-(FULL_FLIP_ANGLE - COVER_ANGLE_TO_RELEASE)));
				this.moveMotor.rotate(decideDirection(CALIBRE_ANGLE));
				this.moveMotor.stop();
			} break;
		}
		currentFlipState = CurrentFlipState.ON_CUBE;
	}
	
	public void flipCubeWithTheSameState() {
		switch (currentFlipState) {
			case RELEASED: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(FULL_FLIP_ANGLE));
				this.moveMotor.setSpeed(RELEASE_SPEED);
				this.moveMotor.rotate(decideDirection(-FULL_FLIP_ANGLE));
				this.moveMotor.stop();
			} break;
			case ON_CUBE: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(FULL_FLIP_ANGLE - COVER_ANGLE_TO_RELEASE - CALIBRE_ANGLE));
				this.moveMotor.rotate(decideDirection(-(FULL_FLIP_ANGLE - COVER_ANGLE_TO_RELEASE)));
				this.moveMotor.rotate(decideDirection(CALIBRE_ANGLE));
				this.moveMotor.stop();
			} break;
		}
	}
	
	public void coverCube() {
		switch (currentFlipState) {
			case RELEASED: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(COVER_ANGLE_TO_RELEASE + CALIBRE_ANGLE));
				this.moveMotor.stop();
			} break;

			case ON_CUBE: break;
		}
		currentFlipState = CurrentFlipState.ON_CUBE;
	}
	
	public void releaseCube() {
		switch (currentFlipState) {
			case RELEASED: break;

			case ON_CUBE: {
				this.moveMotor.setSpeed(FLIP_SPEED);
				this.moveMotor.rotate(decideDirection(-(COVER_ANGLE_TO_RELEASE + CALIBRE_ANGLE)));
				this.moveMotor.stop();
			} break;
		}
		currentFlipState = CurrentFlipState.RELEASED;
	}
	
	public static CurrentFlipState getCurrentFlipState() {
		return currentFlipState;
	}

	@Override
	public void positionCalibration() {
		this.moveMotor.setSpeed(CALIBRATION_SPEED);
		if (!this.changeDirection) this.moveMotor.backward(); else this.moveMotor.forward();
		Delay.msDelay(CALIBRATION_TIME_MOVE);
		this.moveMotor.stop();
		currentFlipState = CurrentFlipState.RELEASED;
	}

	@Override
	public void flt() {
		super.flt();
		currentFlipState = CurrentFlipState.ON_CUBE;
	}
}
