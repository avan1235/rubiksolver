package com.procyk.maciej.rubik.cube.robot;

public interface ICubePositioner {

    void positionCalibration();
    void stop();
    void flt();
}
