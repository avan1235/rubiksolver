package com.procyk.maciej.rubik.cube;

import com.procyk.maciej.rubik.cube.model.Cube;
import com.procyk.maciej.rubik.cube.model.CubeMove;
import com.procyk.maciej.rubik.cube.model.ICubeSolver;
import com.procyk.maciej.rubik.cube.robot.*;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.util.Delay;

import java.util.Arrays;

public class PocketCubeSolver {

	public static final int BEEP_DELAY = 500;

	public static void main(String... args) {
		Button.ESCAPE.addButtonListener(ExitButtonListener.INSTANCE);
		
		Cube cube = Cube.getEmpty();
		
		final CubeRotator rotator = new CubeRotator(Motor.C);
		final CubeFlipper flipper = new CubeFlipper(Motor.A);
		IForCubeChecker forCubeChecker = new DistanceForCubeChecker(SensorPort.S1, 10);
		ICubeScanner scanner = new CubeScanner(SensorPort.S2, rotator, flipper, Motor.B);

		flipper.positionCalibration();
		scanner.positionCalibration();
		while (!forCubeChecker.isCubePresent());

//		CubeSolvingTester tester = new CubeSolvingTester(rotator, flipper);
//		tester.testWithRandomMoves();

		System.out.println("Scan...");
		scanner.scan(cube);
		beepN(2);

		rotator.flt();
		flipper.coverCube();
		flipper.flt();
		scanner.flt();

		try {
			CubeMove[] moves = cube.findSolution();
			System.out.println(Arrays.toString(moves));
			beepN(1);
			CubeSolvingMachine solver = new CubeSolvingMachine(rotator, flipper);
			solver.solveWithMoves(moves);
			beepN(3);
		} catch (ICubeSolver.NoSolutionFound exc) {
			System.out.println("Solution not found...");
		}
		flipper.releaseCube();
		while(true);
	}

	private static class ExitButtonListener implements ButtonListener {
		public static final ExitButtonListener INSTANCE = new ExitButtonListener();
		private ExitButtonListener() { }
		@Override public void buttonReleased(Button b) { }
		@Override public void buttonPressed(Button b) { System.exit(0); }
	}

	private static void beepN(int N) {
		for (int i = 0; i < N; i++) {
			Sound.beep();
			Delay.msDelay(BEEP_DELAY);
		}
	}

	private static CubeMove[] reverseMoves(CubeMove[] moves) {
		CubeMove[] newMoves = new CubeMove[moves.length];
		for (int i = 0; i < moves.length; i++)
			newMoves[i] = moves[moves.length-1-i].getReverse();
		return newMoves;
	}
}
