package com.procyk.maciej.rubik.cube.file;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader implements Closeable, AutoCloseable {
	
	private FileInputStream inputStream;
	private int content;
	private static final int NEW_LINE_CHAR = '\n';
	private static final String EMPTY_LINE = "";
	private String bufferedLine;
	
	public FileReader(String fileName) throws FileNotFoundException {
		File file = new File(fileName);
		inputStream = new FileInputStream(file);
	}
	
	public boolean hasNextLine() throws IOException {
		StringBuilder builder = new StringBuilder();
		while ((content = inputStream.read()) != -1 && content != NEW_LINE_CHAR) {
			builder.append((char) content);
		}
		bufferedLine =  builder.toString();
		return !EMPTY_LINE.equals(bufferedLine);
	}
	
	public String readLine() {
		return bufferedLine;
	}

	@Override
	public void close() throws IOException {
		if (inputStream != null) {
			inputStream.close();
		}
	}
}
