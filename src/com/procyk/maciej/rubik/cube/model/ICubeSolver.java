package com.procyk.maciej.rubik.cube.model;

public interface ICubeSolver {
	CubeMove[] findSolution(Cube cube) throws NoSolutionFound;
	
	public class NoSolutionFound extends Exception {}
}
