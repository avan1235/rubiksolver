package com.procyk.maciej.rubik.cube.model;

public abstract class AbstractCubeSolver implements ICubeSolver {

    protected static int LONGEST_POSSIBLE_SOLUTION = 11;

    public abstract CubeMove[] findSolution(Cube cube) throws NoSolutionFound;
}
