package com.procyk.maciej.rubik.cube.model;

public enum CubeMove {

    F(new int[][]{{0,3,2,1},{10,20,12,7},{11,21,13,4}}) {
        @Override
        public CubeMove getReverse() { return G; }
        @Override
        public CubeMove getCompletion() { return H; }
        @Override
        public String toString() { return "F"; }
    },
    G(new int[][]{{3,0,1,2},{20,10,7,12},{21,11,4,13}}) {
        @Override
        public CubeMove getReverse() { return F; }
        @Override
        public CubeMove getCompletion() { return H; }
        @Override
        public String toString() { return "F'"; }
    },
    H(new int[][]{{3,1},{2,0},{20,7},{12,10},{21,4},{13,11}}) {
        @Override
        public CubeMove getReverse() { return H; }
        @Override
        public CubeMove getCompletion() { return null; }
        @Override
        public String toString() { return "F2"; }
    },
    R(new int[][]{{4,7,6,5},{1,13,17,9},{2,14,18,10}}) {
        @Override
        public CubeMove getReverse() { return T; }
        @Override
        public CubeMove getCompletion() { return Y; }
        @Override
        public String toString() { return "R"; }
    },
    T(new int[][]{{7,4,5,6},{13,1,9,17},{14,2,10,18}}) {
        @Override
        public CubeMove getReverse() { return R; }
        @Override
        public CubeMove getCompletion() { return Y; }
        @Override
        public String toString() { return "R'"; }
    },
    Y(new int[][]{{7,5},{6,4},{13,9},{17,1},{14,10},{18,2}}) {
        @Override
        public CubeMove getReverse() { return Y; }
        @Override
        public CubeMove getCompletion() { return null; }
        @Override
        public String toString() { return "R2"; }
    },
    U(new int[][]{{8,11,10,9},{0,4,18,23},{1,5,19,20}}) {
        @Override
        public CubeMove getReverse() { return I; }
        @Override
        public CubeMove getCompletion() { return O; }
        @Override
        public String toString() { return "U"; }
    },
    I(new int[][]{{11,8,9,10},{4,0,23,18},{5,1,20,19}}) {
        @Override
        public CubeMove getReverse() { return U; }
        @Override
        public CubeMove getCompletion() { return O; }
        @Override
        public String toString() { return "U'"; }
    },
    O(new int[][]{{11,9},{10,8},{4,23},{18,0},{5,20},{19,1}}) {
        @Override
        public CubeMove getReverse() { return O; }
        @Override
        public CubeMove getCompletion() { return null; }
        @Override
        public String toString() { return "U2"; }
    };

    private final int[][] permutations;

    CubeMove(int[][] permutations) {
        this.permutations = permutations;
    }

    public int[][] getPermutations() {
        return permutations;
    }

    public abstract CubeMove getReverse();
    public abstract String toString();
    public abstract CubeMove getCompletion();
    
    public static CubeMove valueFromChar(char c) {
    	switch (c) {
		case 'U': return U;
		case 'I': return I;
		case 'O': return O;
		case 'R': return R;
		case 'T': return T;
		case 'Y': return Y;
		case 'F': return F;
		case 'G': return G;
		case 'H': return H;
    	}
    	throw new IllegalArgumentException();
    }

    public byte toByte() {
        return (byte) this.ordinal();
    }

    public static CubeMove fromByte(byte inputData) {
        return CubeMove.values()[(int) inputData];
    }

    public static CubeMove[] getFromBytes(byte[] inputData) {
        CubeMove[] moves = new CubeMove[inputData.length];
        for (int i = 0; i < inputData.length; i++)
            moves[i] = fromByte(inputData[i]);
        return moves;
    }
}
