package com.procyk.maciej.rubik.cube.model;

import java.util.Arrays;
import java.util.Random;

import javax.microedition.lcdui.Graphics;

import com.procyk.maciej.rubik.cube.model.ICubeSolver.NoSolutionFound;


public class Cube {
    
    private static final Graphics GRAPHICS = new Graphics();
    private static final Random GENERATOR = new Random();
    
    private static final int FIELD_SIZE = 10;
    private static final int FACE_SIZE = 2 * FIELD_SIZE;

	private static final int CUBE_FIELDS = 24;

    private final CubeColor[] cubeColors = new CubeColor[CUBE_FIELDS];
    private final ICubeSolver cubeSolver = new BluetoothSolver();
    
    private Cube() { }

    public void setColor(int cubePosition, CubeColor color) {
        this.cubeColors[cubePosition] = color;
    }

    public static Cube getSolved() {
        Cube cube = new Cube();
        cube.initSolvedFieldS();
        return cube;
    }

    public void shuffle(int changes) {
        for (int i = 0; i < changes; i++)
            this.moveRandomly();
    }

    public void moveRandomly() {
        this.move(CubeMove.values()[GENERATOR.nextInt(CubeMove.values().length)]);
    }

    public void setSolved() {
        this.initSolvedFieldS();
    }
    
    public static Cube getEmpty() {
    	return new Cube();
    }
    
    public boolean isFilled() {
    	for (int i = 0; i < cubeColors.length; i++) {
			if (cubeColors[i] == null)
				return false;
		}
    	return true;
    }

    public byte[] toBytes() {
        byte[] cubeData = new byte[CUBE_FIELDS];
        for (int i = 0; i < CUBE_FIELDS; i++)
            cubeData[i] = cubeColors[i].toByte();
        return cubeData;
    }

    public static Cube fromBytes(byte[] inputData) {
        if (inputData.length != CUBE_FIELDS) throw new IllegalArgumentException("Bad input data to create the cube");
        Cube dataCube = Cube.getEmpty();
        for (int i = 0; i < CUBE_FIELDS; i++)
            dataCube.setColor(i, CubeColor.fromByte(inputData[i]));
        return dataCube;
    }
    
    public String cubeStateRepresentation() {
    	int[] state = new int[cubeColors.length];
    	for (int i = 0; i < state.length; i++)
    		state[i] = cubeColors[i].ordinal();
    	
		int[] positions = new int[6];
		Arrays.fill(positions, -1);
    	int index = 0;
    	for (int i = 0; i < state.length; i++) {
    		if (positions[state[i]] == -1)
				positions[state[i]] = index++;
    	}
    	StringBuilder builder = new StringBuilder();
    	for (int i = 0; i < state.length; i++)
    		builder.append(positions[state[i]]);

    	return builder.toString();
    }

    private void initSolvedFieldS() {
        this.setColor(0, CubeColor.ORANGE);
        this.setColor(1, CubeColor.ORANGE);
        this.setColor(2, CubeColor.ORANGE);
        this.setColor(3, CubeColor.ORANGE);
        this.setColor(4, CubeColor.GREEN);
        this.setColor(5, CubeColor.GREEN);
        this.setColor(6, CubeColor.GREEN);
        this.setColor(7, CubeColor.GREEN);
        this.setColor(8, CubeColor.WHITE);
        this.setColor(9, CubeColor.WHITE);
        this.setColor(10, CubeColor.WHITE);
        this.setColor(11, CubeColor.WHITE);
        this.setColor(12, CubeColor.YELLOW);
        this.setColor(13, CubeColor.YELLOW);
        this.setColor(14, CubeColor.YELLOW);
        this.setColor(15, CubeColor.YELLOW);
        this.setColor(16, CubeColor.RED);
        this.setColor(17, CubeColor.RED);
        this.setColor(18, CubeColor.RED);
        this.setColor(19, CubeColor.RED);
        this.setColor(20, CubeColor.BLUE);
        this.setColor(21, CubeColor.BLUE);
        this.setColor(22, CubeColor.BLUE);
        this.setColor(23, CubeColor.BLUE);
    }

    private void applyPermutation(int[] permutation) {
        assert permutation.length > 0 : "Empty permutation not allowed";
        CubeColor first = cubeColors[permutation[0]];
        for (int i = 0; i < permutation.length-1; i++) {
            cubeColors[permutation[i]] = cubeColors[permutation[i+1]];
        }
        cubeColors[permutation[permutation.length-1]] = first;
    }

    public void move(CubeMove move) {
        for (int[] permutation : move.getPermutations())
            this.applyPermutation(permutation);
    }

    public boolean isSolved() {
        for (int i = 0; i < 24; i += 4) {
            for (int j = 1; j < 4; j++)
                if (cubeColors[i] != cubeColors[i+j])
                    return false;
        }
        return true;
    }

    public CubeMove[] findSolution() throws NoSolutionFound {
        return cubeSolver.findSolution(this);
    }
    
    public void drawCube() {
    	this.clearCube();
		this.drawCube(10, 2);
	}
    
    public void clearCube() {
    	GRAPHICS.clear();
    }
	
    private void drawCube(int x, int y) {
    	drawFace(FACE_SIZE + x, FACE_SIZE + y, 0, 1);
		drawFace(FACE_SIZE + x, y, 4, 1);
		drawFace(x, FACE_SIZE + y, 8, 1);
		drawFace(2 * FACE_SIZE + x, FACE_SIZE + y, 12, 1);
		drawFace(3 * FACE_SIZE + x, FACE_SIZE + y, 16, 1);
		drawFace(2 * FACE_SIZE + x, 2 * FACE_SIZE + y, 20, 1);
	}
    
    private void drawFace(int x, int y, int indexStart, int offset) {
    	drawRectangleWithColor(x, y, cubeColors[indexStart + (offset % 4)]);
    	drawRectangleWithColor(x + FIELD_SIZE, y, cubeColors[indexStart + ((offset + 1) % 4)]);
    	drawRectangleWithColor(x + FIELD_SIZE, y + FIELD_SIZE, cubeColors[indexStart + ((offset + 2) % 4)]);
    	drawRectangleWithColor(x, y + FIELD_SIZE, cubeColors[indexStart + ((offset + 3) % 4)]);
    }
    
    private void drawRectangleWithColor(int x, int y, CubeColor color) {
    	GRAPHICS.drawChar(color != null ? color.toString().charAt(0) : ' ', x+3, y+2, Graphics.TOP | Graphics.LEFT);
    	GRAPHICS.drawRect(x, y, FIELD_SIZE, FIELD_SIZE);
    }
}
