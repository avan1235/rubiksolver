package com.procyk.maciej.rubik.cube.model;

import java.io.IOException;

import com.procyk.maciej.rubik.cube.file.FileReader;

public class DFSBufferedSolver extends DFSSolver {
	private static int LONGEST_POSSIBLE_SOLUTION = 10;
	
	@Override
	public CubeMove[] findSolution(Cube cube) throws NoSolutionFound {
		String cubeRepresentation = cube.cubeStateRepresentation();
		try (FileReader fileReader = new FileReader("data.txt")) {
			String line = null;
			while (fileReader.hasNextLine()) {
				if ((line = fileReader.readLine()).startsWith(cubeRepresentation))
					return movesFromLine(line);
			}
		} catch (IOException e) {
			System.out.println("File not found!");
		}
		
		System.out.println("No buffered solution");
		return super.findSolution(cube);
	}
	
	private static CubeMove[] movesFromLine(String line) {
		try {
			CubeMove[] moves = new CubeMove[11];
			for (int i = 24; i < 35; i++) {
				moves[i-24] = CubeMove.valueFromChar(line.charAt(i));
			}
			return moves;
		} catch (Exception e) {
			return new CubeMove[] {};
		}
	}

	@Override
	protected int getLengthOfLongestSolution() {
		return LONGEST_POSSIBLE_SOLUTION;
	}
}
