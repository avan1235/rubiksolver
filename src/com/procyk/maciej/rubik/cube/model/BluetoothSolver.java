package com.procyk.maciej.rubik.cube.model;

import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class BluetoothSolver extends AbstractCubeSolver {

    private static final byte DATA_BEGIN = '*';
    private static final byte DATA_END = '#';

    @Override
    public CubeMove[] findSolution(Cube cube) throws NoSolutionFound {
        try {
            byte[] buffer = new byte[2 * (2+LONGEST_POSSIBLE_SOLUTION)];
            System.out.println("Wait for device");
            NXTConnection connection = Bluetooth.waitForConnection(0, NXTConnection.RAW);
            System.out.println("Got connection");
            DataInputStream inputStream = connection.openDataInputStream();
            DataOutputStream outputStream = connection.openDataOutputStream();
            System.out.println("Streams opened");
            byte[] cubeData = cube.toBytes();
            for (int i = 0; i < 50; i++) {
                outputStream.write(DATA_BEGIN);
                outputStream.write(cubeData);
                outputStream.write(DATA_END);
            }
            System.out.println("Data sent");
            System.out.println(Arrays.toString(cubeData));
            inputStream.read(buffer, 0, 2 * (2+LONGEST_POSSIBLE_SOLUTION));
            byte[] bufferData = getFinalDataFromBuffer(buffer);
            CubeMove[] solution = CubeMove.getFromBytes(bufferData);
            System.out.println("Data received");
            System.out.println(Arrays.toString(bufferData));
            Bluetooth.closeConnection((byte) 0);
            System.out.println("Connection closed");
            return solution;
        } catch (IOException exc) {
            System.out.println("Caught exception");
        }
        return new CubeMove[] {};
    }

    private static byte[] getFinalDataFromBuffer(byte[] buffer) {
        int b = buffer.length, e = buffer.length;
        for (int i = 0; i < buffer.length; i++)
            if (buffer[i] == DATA_BEGIN) b = Math.min(b, i);
        for (int i = 0; i < buffer.length; i++)
            if (buffer[i] == DATA_END) e = i > b ? Math.min(e, i) : e;
        if (b >= e || b == buffer.length || e == buffer.length) {
            System.out.println(buffer);
            try {
                Thread.sleep(50000);
            } catch (Exception exc) { }
            throw new IllegalArgumentException("Bad data to extract from");
        }
        byte[] data = new byte[e - b - 1];
        for (int i = b+1; i < e; i++)
            data[i-b-1] = buffer[i];
        return data;
    }
}
