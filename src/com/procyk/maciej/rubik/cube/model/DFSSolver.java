package com.procyk.maciej.rubik.cube.model;

import java.util.Arrays;

public class DFSSolver extends AbstractCubeSolver {
	
	private static final CubeMove[] POSSIBLE_MOVES = CubeMove.values();
    private static final int NUMBER_OF_POSSIBLE_MOVES = POSSIBLE_MOVES.length;

	@Override
	public CubeMove[] findSolution(Cube cube) throws NoSolutionFound {
		try {
            CubeMove[] moves = new CubeMove[getLengthOfLongestSolution()];
            findSolutionHelp(0, cube, moves, this);
            throw new NoSolutionFound();
        } catch (Solution exc) {
            return exc.getSolution();
        }
	}
	
	protected int getLengthOfLongestSolution() {
		return LONGEST_POSSIBLE_SOLUTION;
	}

    protected static void findSolutionHelp(int currentDepth, Cube currentCube, CubeMove[] moves, DFSSolver solver) throws Solution {
        if (currentCube.isSolved()) throw new Solution(currentDepth, moves);
        if (currentDepth >= solver.getLengthOfLongestSolution()) return;

        for (int currentMove = 0; currentMove < NUMBER_OF_POSSIBLE_MOVES; currentMove++) {
            if (
                (currentDepth == 0 || 
                    ((moves[currentDepth-1] != POSSIBLE_MOVES[currentMove].getReverse()) 
                  && (moves[currentDepth-1] != POSSIBLE_MOVES[currentMove].getCompletion()) 
                  && (moves[currentDepth-1].getCompletion() != POSSIBLE_MOVES[currentMove]) 
                  && (moves[currentDepth-1] != POSSIBLE_MOVES[currentMove] || POSSIBLE_MOVES[currentMove] == POSSIBLE_MOVES[currentMove].getReverse()))) 
                && (currentDepth < 4 || moves[currentDepth-3] != moves[currentDepth-2] || moves[currentDepth-2] != moves[currentDepth-1] || moves[currentDepth-1] != POSSIBLE_MOVES[currentMove])
            ) {
                moves[currentDepth] = POSSIBLE_MOVES[currentMove];
                currentCube.move(POSSIBLE_MOVES[currentMove]);
                findSolutionHelp(currentDepth+1, currentCube, moves, solver);
                currentCube.move(POSSIBLE_MOVES[currentMove].getReverse());
            }
        }
    }
    
    public static class Solution extends Exception {
        private CubeMove[] solution;

        public Solution(int currentDepth, CubeMove[] savedMoves) {
            this.solution = new CubeMove[currentDepth];
            for (int i = 0; i < currentDepth; i++) {
                this.solution[i] = savedMoves[i];
            }
        }

        public CubeMove[] getSolution() {
            return solution;
        }

        @Override
        public String toString() {
            return Arrays.toString(solution);
        }
    }
    
}
