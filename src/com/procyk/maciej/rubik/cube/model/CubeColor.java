package com.procyk.maciej.rubik.cube.model;

import lejos.nxt.ColorSensor.Color;

public enum CubeColor {
	
    WHITE(new int[]{430, 540}, new int[]{515, 565}, new int[]{360, 440}, new int[]{430, 460}, new int[]{365, 475}, new int[]{445, 500}),
    RED(new int[]{465}, new int[]{505}, new int[]{190}, new int[]{225}, new int[]{195}, new int[]{235}),
    ORANGE(new int[]{500}, new int[]{535}, new int[]{225}, new int[]{270}, new int[]{195}, new int[]{255}),
    BLUE(new int[]{240}, new int[]{300}, new int[]{215}, new int[]{255}, new int[]{345}, new int[]{375}),
    GREEN(new int[]{285}, new int[]{320}, new int[]{300}, new int[]{325}, new int[]{265}, new int[]{295}),
    YELLOW(new int[]{525}, new int[]{560}, new int[]{380}, new int[]{410}, new int[]{340}, new int[]{370});

    private final int[] downRangeR;
    private final int[] upRangeR;
    private final int[] downRangeB;
    private final int[] upRangeB;
    private final int[] downRangeG;
    private final int[] upRangeG;

    CubeColor(int[] downRangeR, int[] upRangeR, int[] downRangeG, int[] upRangeG, int[] downRangeB, int[] upRangeB) {
    	this.downRangeR = downRangeR;
    	this.upRangeR = upRangeR;
    	this.downRangeG = downRangeG;
    	this.upRangeG = upRangeG;
    	this.downRangeB = downRangeB;
    	this.upRangeB = upRangeB;
    }

    public static CubeColor detect(Color toDetect) throws NotDetectedException {
        CubeColor[] colors = CubeColor.values();
        int R = toDetect.getRed(), G = toDetect.getGreen(), B = toDetect.getBlue();
        for (CubeColor color : colors) {
        	for (int i = 0; i < color.downRangeR.length; i++) 
        		if (color.upRangeR[i] >= R && color.downRangeR[i] <= R
        				&& color.upRangeG[i] >= G && color.downRangeG[i] <= G
        				&& color.upRangeB[i] >= B && color.downRangeB[i] <= B) 
                    return color;
        }
        throw new NotDetectedException();
    }

    public byte toByte() {
        return (byte) this.ordinal();
    }

    public static CubeColor fromByte(byte input) {
        return CubeColor.values()[(int) input];
    }

    public static class NotDetectedException extends Exception {}

}
