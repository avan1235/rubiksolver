# RubikSolver for Pocket Rubik Cube in leJOS
Project of robot built with LEGO® Mindstorms NXT 2.0 which solves the Rubik Pocket Cube 2x2x2.
Code written in Java using the leJOS language for NXT with dedicated Java Virtual Machine for robot

## Robot build
Photos to be added...

## Method of finding solution by robot
One can prove that every Pocket Cube can be solved in at most 11 moves. 
Robot tries to find any solution which is shorter or equal to 11 moves by creating
all possible moves paths via DFS searching with some optimizations